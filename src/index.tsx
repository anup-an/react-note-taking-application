import React, { useState } from 'react';
import ReactDOM from 'react-dom';

const App:React.FC = () => {
  const [note, setNote] = useState(""); 
  const [noteArray,setNoteArray]  = useState<string[]>([]);

  const onChangeHandler = (event:React.ChangeEvent<HTMLInputElement>):void => {
    setNote(event.currentTarget.value);
  }

  const onSubmitHandler = (event:React.FormEvent<HTMLFormElement>):void => {
    event.preventDefault();
    setNoteArray([...noteArray,note]);
  }

  const onClickHandler = (index:number) => {
    setNoteArray(noteArray.filter((e,index1) => index1 !== index));
  }


  return (
    <div>
      <div>
        {noteArray.map((element,index) => 
        <li key = {index}>{element} 
          {" "} <button onClick = {() => onClickHandler(index)}>Delete</button>
          <br />
          <br />
        </li>
        )}
      </div>
      <br />
      <div>
        <form onSubmit = {onSubmitHandler}>
          <input type = "text" value={note} onChange = {onChangeHandler} />
          <input type = "submit" value = "Add note" disabled={!note}/>
        </form>
      </div>
    </div>
  )
}
ReactDOM.render(<App />,document.getElementById('root'));
